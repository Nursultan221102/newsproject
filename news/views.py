from django.shortcuts import render
from .models import News, About, Contacts, FAQ
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy


class NewsListView(ListView):
    model = News
    template_name = "news/news_list.html"
    success_url = reverse_lazy('news_detail')

class NewsDetailView(DetailView):
    model = News
    template_name ="news/news_detail.html"

class NewsUpdateView(UpdateView):
    model= News
    template_name = "news/news_update.html"
    fields = ("title", "body")
    success_url = reverse_lazy("news_list")
    login_url = "login"

class NewsDeleteView(DeleteView):
    model = News
    template_name = "news/news_delete.html"
    success_url = reverse_lazy("news_list")
    login_url = "login"

class NewsCreateView(CreateView):
    model= News
    template_name = "news/news_create.html"
    fields = '__all__'
    success_url =reverse_lazy("news_list")
    login_url = "login"

class AboutView(ListView):
    model = About
    template_name = 'about.html'
    context_object_name = 'abouts'

class ContactsView(ListView):
    model = Contacts
    template_name = 'contacts.html'

class FAQView(ListView):
    model = FAQ
    template_name = 'faq.html'