from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse


class News(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(
        get_user_model(),on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

class About(models.Model):
    body = models.TextField()

    def __str__(self):
        return self.body

class Contacts(models.Model):
    contact = models.CharField(max_length=50)

    def __str__(self):
        return self.contact
    
class FAQ(models.Model):
    question = models.TextField()
    answer = models.TextField()

    def __str__(self):
        return self.question
        return self.answer