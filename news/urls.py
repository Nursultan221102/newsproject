from django.urls import path
from news.views import NewsListView, NewsDetailView, NewsUpdateView, NewsDeleteView, NewsCreateView, AboutView, ContactsView, FAQView


urlpatterns = [
    path("", NewsListView.as_view(), name="news_list"),
    path("detail/<int:pk>/", NewsDetailView.as_view(), name="news_detail"),
    path("update/<int:pk>/", NewsUpdateView.as_view(), name="news_update"),
    path("delete/<int:pk>/",NewsDeleteView.as_view(), name="news_delete"),
    path("create/", NewsCreateView.as_view(), name="news_create"),
    path("about/", AboutView.as_view(), name="about"),
    path("contacts/", ContactsView.as_view(), name="contacts"),
    path("FAQ/", FAQView.as_view(), name="faq"),
]