from django.contrib import admin
from .models import News, About, Contacts

admin.site.register(News)
admin.site.register(About)
admin.site.register(Contacts)
